import React from 'react';
import Counter from "../Counter/Counter";
import OrderList from "../OrderList/OrderList";
import '../assets/bootstrap.min.css';

const Orders = ({orderMenu, orders, removeOrder}) => {
    let item;
    const isOrder = orders.every((order, index, array) => array[index].count === 0);

    if (isOrder === false){
        item =
            <div >
                <OrderList removeOrder={removeOrder} orders={orders}/>
                <Counter orders={orders} orderMenu={orderMenu}/>
            </div>

    } else {
        item =
            <div>
                <p>Order is empty</p>
                <p>Please add some items!</p>
            </div>

    }

    return (
        <div className="card d-flex p-4" style={{width: "400px"}}>
            <h2>Order Details:</h2>
            {item}
        </div>
    );
};

export default Orders;