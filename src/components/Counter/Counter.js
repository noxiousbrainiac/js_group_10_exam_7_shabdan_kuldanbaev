import React from 'react';

const Counter = ({orders, orderMenu}) => {
    let totalPrice = 0;

    for (let i = 0; i < orders.length; i++) {
        if (orders[i].name === orderMenu[i].name){
            totalPrice += (orders[i].count * orderMenu[i].price)
        }
    }
    return (
        <h3>Total price: {totalPrice}</h3>
    );
};

export default Counter;