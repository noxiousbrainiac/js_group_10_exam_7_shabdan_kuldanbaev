import React from 'react';
import '../assets/bootstrap.min.css';
import MenuItem from "./MenuItem";

const AddOrder = ({orderMenu, addOrder}) => {
    return (
        <div className="card d-flex p-4" style={{width: "600px"}}>
            <h2>Add items:</h2>
            <div className="d-flex flex-wrap justify-content-evenly">
                {orderMenu.map((order, index) => {
                    return (
                        <MenuItem key={index} order={order} addOrder={addOrder} id={index}/>
                    )
                })}
            </div>
        </div>
    );
};

export default AddOrder;