import {useState} from 'react';
import Orders from "./components/Orders/Orders";
import AddOrder from "./components/AddOrder/AddOrder";
import './components/assets/bootstrap.min.css';
import chicken from './components/assets/chicken.png';
import coffee from './components/assets/coffee.png';
import cola from './components/assets/cola.png';
import donut from './components/assets/donut.png';
import doner from './components/assets/doner.png';
import fries from './components/assets/fries.png';
import hamburger from './components/assets/hamburger.png';
import pizza from './components/assets/pizza.png';
import {nanoid} from "nanoid";


const App = () => {
  const orderMenu = [
    {name: "Cola", price: 30, img: cola},
    {name: "Coffee", price: 30, img: coffee},
    {name: "Donut", price: 50, img: donut},
    {name: "Fries", price: 60, img: fries},
    {name: "Hamburger", price: 100, img: hamburger},
    {name: "Doner-Kebab", price: 120, img: doner},
    {name: "Pizza", price: 290, img: pizza},
    {name: "Chicken", price: 320, img: chicken}
  ];

  const [orders, setOrders] = useState([
    {name: "Cola", count: 0, id: nanoid()},
    {name: "Coffee", count: 0, id: nanoid()},
    {name: "Donut", count: 0, id: nanoid()},
    {name: "Fries", count: 0, id: nanoid()},
    {name: "Hamburger", count: 0, id: nanoid()},
    {name: "Doner-Kebab", count: 0, id: nanoid()},
    {name: "Pizza", count: 0, id: nanoid()},
    {name: "Chicken", count: 0, id: nanoid()},
  ]);

  const addOrder = name => {
    setOrders(orders.map(order => {
      if (order.name === name) {
        return {
          ...order,
          count: order.count + 1
        }
      }
      return order;
    }))
  };

  const removeOrder = name => {
    setOrders(orders.map(order => {
      if (order.name === name) {
        return {
          ...order,
          count: 0
        }
      }
      return order;
    }))
  };

    return (
        <div className="container">
          <div className="d-flex m-5 p-1 justify-content-evenly">
            <Orders
                orders={orders}
                orderMenu={orderMenu}
                removeOrder={removeOrder}
            />
            <AddOrder orderMenu={orderMenu} addOrder={addOrder}/>
          </div>
        </div>
    );
};

export default App;