import React from 'react';

const MenuItem = ({order, addOrder}) => {
    return (
        <div
            className="card flex-row p-3 mb-3 justify-content-evenly"
            style={{width: "250px", cursor: "pointer"}}
            onClick={()=> addOrder(order.name)}
        >
            <div style={{width: "90px"}}>
                <img src={order.img} alt="Fast food" width="100%"/>
            </div>
            <div>
                <h4>{order.name}</h4>
                <p>Price: {order.price} som</p>
            </div>
        </div>
    );
};

export default MenuItem;