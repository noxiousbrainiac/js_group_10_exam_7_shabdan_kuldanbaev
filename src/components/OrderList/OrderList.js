import React from 'react';
import OrderItem from "./OrderItem";


const OrderList = ({orders, removeOrder}) => {
    return (
        <ul className="list-group">
            {orders.map((order, i) => {
                if (order.count !== 0){
                    return (
                        <OrderItem key={i} order={order} removeOrder={removeOrder}/>
                    )
                }
                return null;
            })}
        </ul>
    );
};

export default OrderList;